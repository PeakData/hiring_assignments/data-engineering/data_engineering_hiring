# Data Engineering @ PeakData

The goal of this task is to recreate a minimal working scenario of data engineers at PeakData, in order to see the following:

- Ability to manipulate data using modern Python data tools
- Knowledge of Python best practices
- Ability to decompose tasks and high-level thinking
- Basic programming principles

## Task

Given a list of medical publications, provide a list of unique authors and how many publications have they written.

Note that authors can appear in multiple publications and may have slight differences in their names, initials, etc. between articles.

In addition to your code, we expect a `README.md` containing:
- How to build and run the code
- Documentation on your approach, i.e. what did you do and why?
- A reporting of potential failure points and bottlenecks
- An accounting of the remaining steps needed before deploying your code to a production system

The code should be locally runnable on a UNIX-flavored OS (MacOS, Linux).

### Input

The input publications can be found in `publications.csv.gz` in the repository. The data is tabular and in following format:

| publication_uuid | journal | pubdate | title | abstract | authors | affiliations |
| ---------------- | ------- | ------- | ----- | -------- | ------- | ------------ |
| 1 | Journal 1 | 2020-01-01 06:00:00 | Title of publication 1 | Abstract of publication 1 | ['John Doe', 'Douglas Glover', 'Truman Young', 'Diana Jennings', 'Brigham Stevenson', 'Trina Silva', 'Bill Frost', 'Herbert Möller'] | ['Cambridge University Hospital, Hills Rd, Cambridge, United Kingdom', 'Milton Keynes University Hospital, Standing Way, Eaglestone, Milton Keynes, United Kingdom'] |
| 2 | Journal 2 | 2021-01-01 00:00:00 | Title of publication 2 | Abstract of publication 2 | ['J Doe', 'D Jennings', 'B Stevenson', 'T Silva', 'J Covington', 'T Goodman', 'S Pena', 'H Moeller'] | ['Milton Keynes University Hospital, Standing Way, Eaglestone, Milton Keynes, United Kingdom', 'Nottingham University Hospital, City Hospital, Hucknall Rd, Nottingham, United Kingdom'] |
| 3 | die Fachzeitschrift | 2000-12-12 12:12:12 | Titel 3 | Abstrakt 3 | ['Jenice Doe', 'Harman Sauer', 'Lies Hoffmann', 'Katrin Schmidt'] |  |

### Expected output

If we were given *ONLY* the exemplary publications as input, we would expect following result (ideally):

| firstname | lastname  | no_of_publications |
| --------- | --------- | ------------------ |
| John      | Doe       | 2 |
| Douglas   | Glover    | 1 |
| Truman    | Young     | 1 |
| Diana     | Jennings  | 2 |
| Brigham   | Stevenson | 2 |
| Trina     | Silva     | 2 |
| Bill      | Frost     | 1 |
| Herbert   | Möller    | 2 |
| J         | Covington | 1 |
| T         | Goodman   | 1 |
| S         | Pena      | 1 |
| Jenice    | Doe       | 1 |
| Harman    | Sauer     | 1 |
| Lies      | Hoffmann  | 1 |
| Katrin    | Schmidt   | 1 |


## Time is limited

We understand your time is precious and would not want you to spend more than `5 hours over the span of one week`.
It is OK if the challenge is not completed or your results are not perfect - try to prioritize by what you think is most important. 
Tell us what motivated your choices, how you tackled the task, what you would do differently were you given more time, what you would differently a second time around, etc.

## Submission

Publish the results of your work on a repository, share its link with us via email to `michael@peakdata.com` and make sure that it is public.
We will confirm your submission and get back to you about next steps within 1 week.
